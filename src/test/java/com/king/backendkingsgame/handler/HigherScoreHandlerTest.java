package com.king.backendkingsgame.handler;

import com.king.backendkingsgame.dao.LevelDAO;
import com.king.backendkingsgame.dao.impl.LevelDAOImpl;
import com.king.backendkingsgame.entity.Level;
import com.king.backendkingsgame.handler.impl.HigherScoresHandlerImpl;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class HigherScoreHandlerTest extends BaseHandlerTest {
    @Test
    public void givenSystemDoesNotHaveHigherScoreHandler_whenCreateIt_thenReturnAnInstanceOfHigherScoreHandlerImpl() {
        //given/when
        HigherScoresHandler higherScoresHandler = new HigherScoresHandlerImpl();

        //then
        assertThat(higherScoresHandler, instanceOf(HigherScoresHandlerImpl.class));
    }

    @Test
    public void givenHigherScoreHandler_whenHandlerRequestIsCalled_thenVerifyIfMethodsFromHttpExchangeWasCalled() throws URISyntaxException {
        //given
        HigherScoresHandler higherScoresHandler = new HigherScoresHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("GET");
        given(httpExchange.getRequestURI()).willReturn(new URI("/2/highscorelist"));
        given(httpExchange.getResponseHeaders()).willReturn(new Headers());
        LevelDAO levelDAO = LevelDAOImpl.getInstance();
        levelDAO.save(new Level(2));
        mockHandlerUtil();

        //when
        higherScoresHandler.handlerRequest(httpExchange);

        //then
        verify(httpExchange, times(1)).getRequestMethod();
        verify(httpExchange, times(1)).getRequestURI();
        verify(httpExchange, times(1)).getResponseHeaders();
    }
}
