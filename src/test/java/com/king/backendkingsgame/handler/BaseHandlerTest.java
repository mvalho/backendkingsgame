package com.king.backendkingsgame.handler;

import com.sun.net.httpserver.HttpExchange;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Ignore;

@Ignore
public class BaseHandlerTest {

    protected void mockHandlerUtil() {
        new MockUp<HandlerUtil>() {
            @Mock
            public void produceResponse(HttpExchange httpExchange, String response) {

            }
        };
    }
}
