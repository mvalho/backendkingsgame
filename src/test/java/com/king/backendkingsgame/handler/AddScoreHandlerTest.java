package com.king.backendkingsgame.handler;

import com.king.backendkingsgame.handler.impl.AddScoreHandlerImpl;
import com.sun.net.httpserver.HttpExchange;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class AddScoreHandlerTest {
    @Test
    public void givenSystemDoesNotHaveAddScoreHandler_whenCreateIt_thenReturnAnInstanceOfAddScoreHandlerImpl() {
        //given/when
        AddScoreHandler addScoreHandler = new AddScoreHandlerImpl();

        //then
        assertThat(addScoreHandler, instanceOf(AddScoreHandlerImpl.class));
    }

    @Test
    public void givenAddScoreHandler_whenHandlerRequestIsCalled_thenVerifyIfMethodsFromHttpExchangeWasCalled() throws URISyntaxException {
        //given
        AddScoreHandler addScoreHandler = new AddScoreHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("POST");
        given(httpExchange.getRequestURI()).willReturn(new URI("/2/score?sessionkey=ASDUICSNDK"));
        mockHandlerUtil();

        //when
        addScoreHandler.handlerRequest(httpExchange);

        //then
        verify(httpExchange, times(1)).getRequestMethod();
        verify(httpExchange, times(1)).getRequestURI();
    }

    private void mockHandlerUtil() {
        new MockUp<HandlerUtil>() {
            @Mock
            public void produceResponse(HttpExchange httpExchange) {

            }
        };
    }
}
