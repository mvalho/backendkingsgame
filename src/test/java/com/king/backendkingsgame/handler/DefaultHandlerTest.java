package com.king.backendkingsgame.handler;

import com.king.backendkingsgame.handler.impl.DefaultHandlerImpl;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class DefaultHandlerTest {
    @Test
    public void givenSystemDoesNotHaveDefaultHandler_whenCreateIt_thenReturnAnInstanceOfDefaultHandlerImpl() {
        //given/when
        DefaultHandler defaultHandler = new DefaultHandlerImpl();

        //then
        assertThat(defaultHandler, instanceOf(DefaultHandlerImpl.class));
    }

    @Test
    public void givenDefaultHandler_whenHandlerRequestIsCalled_thenVerifyIfMethodsFromHttpExchangeWasCalled() throws IOException {
        //given
        DefaultHandler defaultHandler = new DefaultHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream outputStream = mock(OutputStream.class);
        given(httpExchange.getResponseBody()).willReturn(outputStream);

        //when
        defaultHandler.handlerRequest(httpExchange);

        //then
        verify(httpExchange, times(1)).sendResponseHeaders(anyInt(), anyLong());
        verify(httpExchange, times(1)).getResponseBody();
    }

}
