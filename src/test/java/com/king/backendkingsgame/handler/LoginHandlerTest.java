package com.king.backendkingsgame.handler;

import com.king.backendkingsgame.handler.impl.LoginHandlerImpl;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class LoginHandlerTest extends BaseHandlerTest {
    @Test
    public void givenSystemDoesNotHaveLoginHandler_whenCreateIt_thenReturnAnInstanceOfLoginHandlerImpl() {
        //given/when
        LoginHandler loginHandler = new LoginHandlerImpl();

        //then
        assertThat(loginHandler, instanceOf(LoginHandlerImpl.class));
    }

    @Test
    public void givenLoginHandler_whenHandlerRequestIsCalled_thenVerifyIfMethodsFromHttpExchangeWasCalled() throws URISyntaxException {
        //given
        LoginHandler loginHandler = new LoginHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("GET");
        given(httpExchange.getRequestURI()).willReturn(new URI("/4711/login"));
        mockHandlerUtil();

        //when
        loginHandler.handlerRequest(httpExchange);

        //then
        verify(httpExchange, times(1)).getRequestMethod();
        verify(httpExchange, times(1)).getRequestURI();
    }

}
