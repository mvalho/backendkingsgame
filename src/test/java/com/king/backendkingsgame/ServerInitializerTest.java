package com.king.backendkingsgame;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class ServerInitializerTest {

    @BeforeClass
    public static void init() throws IOException {
        ServerInitializer.main(new String[]{});
    }

    @Test
    public void givenServerInitializer_whenServerIsStarted_thenResponseShouldBe404() throws IOException {
        //when
        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/").openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.disconnect();

        //then
        assertThat(responseCode, equalTo(404));
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndLoginIsCalled_thenResponseCodeIs200AndResponseBodyIsStringWith10Digits() throws IOException {
        //when
        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/100/login").openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        String response = extractResponse(con);
        con.disconnect();

        //then
        assertThat(responseCode, equalTo(200));
        assertThat(response.length(), equalTo(10));
        assertThat(response, matchesPattern("[A-Z]{10}"));
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndAddScoreIsCalled_thenResponseCodeIs200AndResponseBodyIsEmpty() throws IOException {
        //when
        String userId = "101";
        String sessionKey = login(userId);

        String levelId = "1000";
        HttpURLConnection con = addScore(sessionKey, levelId, "1500", "POST");
        int responseCode = con.getResponseCode();
        String response = extractResponse(con);
        con.disconnect();

        //then
        assertThat(responseCode, equalTo(200));
        assertThat(response.length(), equalTo(0));
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndHigherScoreIsCalled_thenResponseCodeIs200AndResponseBodyListOfScoresAndContentTypeCSV() throws IOException {
        //when
        String levelId = "1001";
        generateUserScores(levelId);

        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/" + levelId + "/highscorelist").openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        String contentType = con.getContentType();
        String response = extractResponse(con);
        con.disconnect();

        //then
        assertThat(responseCode, equalTo(200));
        assertThat(contentType, equalTo("text/csv"));
        assertThat(response, equalTo("103=1251,102=1250,104=1249"));

    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndLoginIsCalled_thenResponseCodeIs405() throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/100/login").openConnection();
        con.setRequestMethod("PUT");
        int responseCode = con.getResponseCode();
        con.disconnect();

        assertThat(responseCode, equalTo(405));
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndAddScoreIsCalled_thenResponseCodeIs405() throws IOException {
        //when
        String levelId = "1000";
        HttpURLConnection con = addScore("ADSASDASDQ", levelId, "1500", "PUT");
        int responseCode = con.getResponseCode();
        con.disconnect();

        //then
        assertThat(responseCode, equalTo(405));
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndHigherScoreIsCalled_thenResponseCodeIs405() throws IOException {
        //when
        String levelId = "1001";
        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/" + levelId + "/highscorelist").openConnection();
        con.setRequestMethod("POST");
        int responseCode = con.getResponseCode();

        //then
        assertThat(responseCode, equalTo(405));
    }

    private void generateUserScores(String levelId) throws IOException {
        String userId1 = "102";
        String userId2 = "103";
        String userId3 = "104";

        String sessionKey1 = login(userId1);
        String sessionKey2 = login(userId2);
        String sessionKey3 = login(userId3);

        HttpURLConnection connection1 = addScore(sessionKey1, levelId, "1250", "POST");
        connection1.getInputStream().close();
        HttpURLConnection connection2 = addScore(sessionKey2, levelId, "1251", "POST");
        connection2.getInputStream().close();
        HttpURLConnection connection3 = addScore(sessionKey3, levelId, "1249", "POST");
        connection3.getInputStream().close();
    }

    private HttpURLConnection addScore(String sessionKey, String levelId, String score, String requestMethod) throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL("http://localhost:8081/" + levelId + "/score?sessionkey=" + sessionKey).openConnection();
        con.setDoOutput(true);
        con.setRequestMethod(requestMethod);
        setBody(score, con);

        return con;
    }

    private String login(String userId) throws IOException {
        HttpURLConnection loginCon = (HttpURLConnection) new URL("http://localhost:8081/" + userId + "/login").openConnection();
        loginCon.setRequestMethod("GET");
        return extractResponse(loginCon);
    }

    private void setBody(String score, HttpURLConnection con) throws IOException {
        OutputStream os = con.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
        osw.write(score);
        osw.flush();
        osw.close();
        os.close();
    }

    private String extractResponse(HttpURLConnection con) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        return content.toString();
    }
}
