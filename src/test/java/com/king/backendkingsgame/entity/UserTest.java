package com.king.backendkingsgame.entity;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class UserTest {
    @Test
    public void givenANewUser_whenUserIsCreated_thenLoggedAtAndTokenExpiredIsInitialized() {
        //given/when
        User user = new User(5000, "UI5000TKN");

        //Then
        assertThat(user.getLoggedAt(), is(instanceOf(LocalDateTime.class)));
        assertThat(user.isTokenExpired(), is(false));
    }

    @Test
    public void givenACreatedUser_whenIsTokenExpired_thenItNeedsToReturnTrueIfLoggedAtIsBiggerThan10Minutes() {
        //given
        User user = new User(5001, "UI5001TKN");

        //when
        LocalDateTime loggedAt = LocalDateTime.now().minusMinutes(11L);
        user.setLoggedAt(loggedAt);

        //Then
        assertThat(user.isTokenExpired(), is(true));
    }
}
