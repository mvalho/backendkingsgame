package com.king.backendkingsgame.entity;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class LevelTest {
    @Test
    public void givenANewLevel_whenAddNewUserScore_thenTheNewScoreNeedToBeInsertedInAInternalList() {
        //Given
        Level level = new Level(5001);

        //when
        User user = new User(2000, "UI2000TKN");
        Score score = new Score(4518);
        UserScore userScore = new UserScore(user, score);
        level.addUserScore(userScore);

        //then
        assertThat(level.getUserScores(), is(notNullValue()));
        assertThat(level.getUserScores(), is(not(empty())));
        assertThat(level.getUserScores().get(0), Matchers.is(userScore));
    }

    @Test
    public void givenExistedLevelWithSomeScores_whenAddNewUserScore_thenTheUserScoreListMustBeOrderedByHigherScores() {
        //given
        Level level = new Level(50002);
        User user1 = new User(2001, "UI2001TKN");
        User user2 = new User(2002, "UI2002TKN");
        User user3 = new User(2003, "UI2003TKN");
        UserScore userScore1 = new UserScore(user1, new Score(122));
        UserScore userScore2 = new UserScore(user2, new Score(754));
        UserScore userScore3 = new UserScore(user3, new Score(250));
        level.addUserScore(userScore1);
        level.addUserScore(userScore2);
        level.addUserScore(userScore3);

        //when
        UserScore userScore4 = new UserScore(new User(2004, "UI2004TKN"), new Score(1472));
        level.addUserScore(userScore4);

        //then
        assertThat(level.getUserScores(), hasSize(4));
        assertThat(level.getUserScores().get(0), Matchers.is(userScore4));
    }

    @Test
    public void givenExistedLevelWithSomeScores_whenAddNewUserScoreLowerThanTheExistedOne_thenTheNewScoreMostNotBeAdded() {
        //given
        Level level = new Level(50002);
        User user1 = new User(2001, "UI2001TKN");
        User user2 = new User(2002, "UI2002TKN");
        User user3 = new User(2003, "UI2003TKN");
        UserScore userScore1 = new UserScore(user1, new Score(122));
        UserScore userScore2 = new UserScore(user2, new Score(754));
        UserScore userScore3 = new UserScore(user3, new Score(250));
        level.addUserScore(userScore1);
        level.addUserScore(userScore2);
        level.addUserScore(userScore3);

        //when
        UserScore userScore4 = new UserScore(user1, new Score(120));
        level.addUserScore(userScore4);

        //then
        assertThat(level.getUserScores(), hasSize(3));
    }
}
