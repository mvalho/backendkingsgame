package com.king.backendkingsgame.dao;


import com.king.backendkingsgame.dao.impl.UserDAOImpl;
import com.king.backendkingsgame.entity.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class UserDAOTest {
    @Test
    public void shouldImplementTheUserDAO() {
        UserDAO userDAO = UserDAOImpl.getInstance();

        assertThat(userDAO, is(notNullValue()));
        assertThat(userDAO, is(instanceOf(UserDAOImpl.class)));
    }

    @Test
    public void givenUserDAOAndNewUser_whenUseTheSaveMethod_thenItNeedToSuccessfullySaveTheUserWithIdAndSessionID() {
        //given
        User user = new User(1, "UIASDLSK");
        UserDAO userDAO = UserDAOImpl.getInstance();

        //when
        userDAO.save(user);
        User retrievedUser = userDAO.findByID(user.getId());

        //then
        assertThat(retrievedUser, is(notNullValue()));
        assertThat(retrievedUser.getId(), is(1));
        assertThat(retrievedUser.getSessionKey(), is("UIASDLSK"));
    }

    @Test
    public void givenUserDAOAListOfThreads_whenThreadsAreStarted_ThenItShouldSuccessfullySaveAllUsers() {
        UserDAO userDAO = UserDAOImpl.getInstance();
        List<Thread> threadList = new ArrayList<>();
        for (AtomicInteger i = new AtomicInteger(); i.get() < 1000; i.getAndIncrement()) {
            threadList.add(new Thread(() -> {
                User user = new User(i.getAndIncrement(), "UIASDLSK" + i);
                userDAO.save(user);
            }));
        }

        threadList.forEach(thread -> {
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        assertThat(userDAO.size(), greaterThan(1000));
    }

    @Test
    public void givenUserDAO_whenSearchBySessionKey_thenItNeedToReturnAUserByItsSessionKey() {
        //given
        UserDAO userDAO = UserDAOImpl.getInstance();
        User user = new User(1001, "UI1001TKN");
        userDAO.save(user);

        //when
        User foundBySessionKey = userDAO.findBySessionKey("UI1001TKN");

        //then
        assertThat(foundBySessionKey, equalTo(user));
        assertThat(foundBySessionKey.getSessionKey(), equalTo("UI1001TKN"));
    }
}
