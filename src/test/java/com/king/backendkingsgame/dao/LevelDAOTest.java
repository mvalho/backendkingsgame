package com.king.backendkingsgame.dao;

import com.king.backendkingsgame.dao.impl.LevelDAOImpl;
import com.king.backendkingsgame.entity.Level;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class LevelDAOTest {
    @Test
    public void givenSystemStillDontHaveLevelDAO_whenCreateIt_thenTheInstanceMustBeLevelDAOImpl() {
        LevelDAO levelDAO = LevelDAOImpl.getInstance();

        assertThat(levelDAO, is(instanceOf(LevelDAOImpl.class)));
    }

    @Test
    public void givenLevelDAOAndNewLevel_whenCallSaveMethod_thenLevelShouldBePersisted() {
        //given
        LevelDAO levelDAO = LevelDAOImpl.getInstance();
        Level expectedLevel = new Level(1000);

        //when
        levelDAO.save(expectedLevel);

        //then
        Level savedLevel = levelDAO.findById(expectedLevel.getId());
        assertThat(savedLevel, is(expectedLevel));
    }

}
