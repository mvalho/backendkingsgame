package com.king.backendkingsgame.service;

import com.king.backendkingsgame.dao.UserDAO;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.service.impl.LoginServiceImpl;
import org.junit.Test;
import org.mockito.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class LoginServiceTest {
    @Test
    public void givenSystemStillDoesNotHaveLoginService_whenCreateId_theTheInstanceMustBeLoginServiceImpl() {
        //given/when
        LoginService loginService = new LoginServiceImpl(null);

        //then
        assertThat(loginService, is(instanceOf(LoginServiceImpl.class)));
    }

    @Test
    public void givenLoginServiceAndANewUserId_whenLogin_thenTheSessionKeyAndLoggedAtMustBeSet() {
        //given
        LoginService loginService = new LoginServiceImpl(null);
        int newUserId = 4001;

        //when
        User user = loginService.login(newUserId);

        //then
        assertThat(user, notNullValue());
        assertThat(user.getId(), equalTo(newUserId));
        assertThat(user.getSessionKey().length(), is(10));
        assertThat(user.getSessionKey(), matchesPattern("[A-Z]+"));
    }

    @Test
    public void givenLoginServiceAndExistingUserID_whenLogin_thenTheSessionKeyAndLoggedAtMustBeReSet() {
        //given
        LoginService loginService = new LoginServiceImpl(null);
        int existedUserID = 4002;
        User firstLogin = loginService.login(4002);

        //when
        User secondLogin = loginService.login(existedUserID);

        assertThat(secondLogin.getSessionKey(), not(equalTo(firstLogin.getSessionKey())));
    }

    @Test
    public void givenLoginServiceAndUserDAO_whenLogin_thenTheUserNeedsToBePersisted() {
        //given
        UserDAO userDAO = mock(UserDAO.class);
        LoginService loginService = new LoginServiceImpl(userDAO);
        int newUserId = 4003;

        //when
        User saved = loginService.login(newUserId);

        //then
        verify(userDAO, times(1)).save(Matchers.any(User.class));
        assertThat(saved.getId(), equalTo(newUserId));
    }

    @Test
    public void givenLoginServiceAndUserDAO_whenGetUserBySessionKey_thenReturnUser() {
        UserDAO userDAO = mock(UserDAO.class);
        LoginService loginService = new LoginServiceImpl(userDAO);
        String sessionKey = "UIASDQWETKN";
        User user = new User(8001, sessionKey);
        given(userDAO.findBySessionKey(sessionKey)).willReturn(user);

        User saved = loginService.getUserBySessionKey(sessionKey);

        assertThat(saved, equalTo(user));
    }
}
