package com.king.backendkingsgame.service;

import com.king.backendkingsgame.dao.LevelDAO;
import com.king.backendkingsgame.dao.UserDAO;
import com.king.backendkingsgame.entity.Level;
import com.king.backendkingsgame.entity.Score;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.entity.UserScore;
import com.king.backendkingsgame.service.impl.LevelServiceImpl;
import com.king.backendkingsgame.service.impl.LoginServiceImpl;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

public class LevelServiceTest {
    @Test
    public void givenSystemStillDoesNotHaveLevelService_whenCreateIt_thenTheInstanceMustBeLevelServiceImpl() {
        //given/when
        LevelService levelService = new LevelServiceImpl(null, null);

        //then
        assertThat(levelService, instanceOf(LevelServiceImpl.class));
    }

    @Test
    public void givenLevelServiceAndUserDAO_whenCheckSessionKey_thenReturnFalse() {
        //given
        UserDAO userDAO = mock(UserDAO.class);
        LoginService loginService = new LoginServiceImpl(userDAO);
        User loggedUser = loginService.login(6001);
        given(userDAO.findBySessionKey(anyString())).willReturn(loggedUser);

        LevelService levelService = new LevelServiceImpl(userDAO, null);

        //when
        boolean isExpiredKey = levelService.isSessionKeyInvalid(loggedUser.getSessionKey());

        assertThat(isExpiredKey, is(false));
    }

    @Test
    public void givenLevelServiceAndUserDAO_whenCheckSessionKey_thenReturnTrue() {
        //given
        UserDAO userDAO = mock(UserDAO.class);
        LoginService loginService = new LoginServiceImpl(userDAO);
        User loggedUser = loginService.login(6001);
        loggedUser.setLoggedAt(LocalDateTime.now().minusMinutes(11));
        LevelService levelService = new LevelServiceImpl(userDAO, null);
        given(userDAO.findBySessionKey(anyString())).willReturn(loggedUser);

        //when
        boolean isExpiredKey = levelService.isSessionKeyInvalid(loggedUser.getSessionKey());

        //then
        assertThat(isExpiredKey, is(true));
    }

    @Test
    public void givenLevelServiceUserDAOAndLevelDAO_whenAddNewScoreWithValidSessionKeyANewLevel_thenAddNewScoreForNewLevel() {
        //given
        Level expectedLevel = new Level(101);
        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(null);
        given(levelDAO.save(any(Level.class))).willReturn(expectedLevel);

        LoginService loginService = new LoginServiceImpl(null);
        User loggedUser = loginService.login(6002);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        levelService.addLevelUserScore(101, loggedUser, 1500);

        //then
        verify(levelDAO, times(1)).findById(101);
        verify(levelDAO, times(2)).save(expectedLevel);
    }

    @Test
    public void givenLevelServiceUserDAOAndLevelDAO_whenAddNewScoreWithValidSessionKeyInExistedLevel_thenAddNewScoreForNewLevel() {
        //given
        Level expectedLevel = new Level(102);
        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LoginService loginService = new LoginServiceImpl(null);
        User loggedUser = loginService.login(6003);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        levelService.addLevelUserScore(102, loggedUser, 1320);

        //then
        verify(levelDAO, times(1)).findById(102);
        verify(levelDAO, times(1)).save(expectedLevel);
    }

    @Test
    public void givenLevelServiceLevelDAO_whenGetHighScoreByLevel_thenReturnEmptyListOfHigherScore() {
        //given
        Level expectedLevel = new Level(103);
        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(103);

        //then
        assertThat(higherScoreByLevel, notNullValue());
        assertThat(higherScoreByLevel, hasSize(0));
    }

    @Test
    public void givenLevelServiceLevelDAO_whenGetHighScoreByLevel_thenReturnListOfHigherScoreWithOneItem() {
        //given
        Level expectedLevel = new Level(104);

        User user = new User(6005, "UI6005TKN");
        Score score = new Score(1501);
        UserScore userScore = new UserScore(user, score);
        expectedLevel.addUserScore(userScore);

        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(105);

        //then
        assertThat(higherScoreByLevel, notNullValue());
        assertThat(higherScoreByLevel, hasSize(1));
    }

    @Test
    public void givenLevelServiceLevelDAO_whenGetHighScoreByLevel_thenReturnListOfFirst15HigherScore() {
        //given
        Level expectedLevel = new Level(106);

        for (int i = 1; i <= 16; i++) {
            User user = new User(6000 + i, "UI600"+ i +"TKN");
            UserScore userScore = new UserScore(user, new Score(1500 + i));
            expectedLevel.addUserScore(userScore);
        }

        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(106);

        //then
        assertThat(higherScoreByLevel, notNullValue());
        assertThat(higherScoreByLevel, hasSize(15));
    }


    @Test
    public void givenLevelServiceLevelDAO_whenGetHighScoreByLevel_thenReturnListOfFirst15HigherScoreOrderedByDescendingPoints() {
        //given
        Level expectedLevel = new Level(107);

        for (int i = 1; i <= 16; i++) {
            User user = new User(7000 + i, "UI700"+ i +"TKN");
            UserScore userScore = new UserScore(user, new Score(1500 * i));
            expectedLevel.addUserScore(userScore);
        }

        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(106);

        //then
        assertThat(higherScoreByLevel, notNullValue());
        assertThat(higherScoreByLevel, hasSize(15));

        assertThat(higherScoreByLevel.get(0).getScore().getPoints(), equalTo(1500*16));
        assertThat(higherScoreByLevel.get(1).getScore().getPoints(), equalTo(1500*15));
        assertThat(higherScoreByLevel.get(2).getScore().getPoints(), equalTo(1500*14));
        assertThat(higherScoreByLevel.get(3).getScore().getPoints(), equalTo(1500*13));
        assertThat(higherScoreByLevel.get(4).getScore().getPoints(), equalTo(1500*12));
        assertThat(higherScoreByLevel.get(5).getScore().getPoints(), equalTo(1500*11));
        assertThat(higherScoreByLevel.get(6).getScore().getPoints(), equalTo(1500*10));
        assertThat(higherScoreByLevel.get(7).getScore().getPoints(), equalTo(1500*9));
        assertThat(higherScoreByLevel.get(8).getScore().getPoints(), equalTo(1500*8));
        assertThat(higherScoreByLevel.get(9).getScore().getPoints(), equalTo(1500*7));
        assertThat(higherScoreByLevel.get(10).getScore().getPoints(), equalTo(1500*6));
        assertThat(higherScoreByLevel.get(11).getScore().getPoints(), equalTo(1500*5));
        assertThat(higherScoreByLevel.get(12).getScore().getPoints(), equalTo(1500*4));
        assertThat(higherScoreByLevel.get(13).getScore().getPoints(), equalTo(1500*3));
        assertThat(higherScoreByLevel.get(14).getScore().getPoints(), equalTo(1500*2));
    }


    @Test
    public void givenLevelServiceLevelDAO_whenGetHighScoreByLevel_thenReturnListOfFirst15HigherScoreNotRepeatingUser() {
        //given
        Level expectedLevel = new Level(107);

        User user = new User(6007, "UI6007TKN");
        for (int i = 1; i <= 5; i++) {
            UserScore userScore = new UserScore(user, new Score(1500 * i));
            expectedLevel.addUserScore(userScore);
        }

        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(expectedLevel);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);

        //when
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(106);

        //then
        assertThat(higherScoreByLevel, notNullValue());
        assertThat(higherScoreByLevel, hasSize(1));
    }

    @Test
    public void givenLevelService_whenTransformIntoCSV_thenReturnStringInCSVFormat() {
        //given
        String expectedString = createExpectedCsvString();
        Level level = new Level(108);

        for (int i = 1; i <= 16; i++) {
            User user = new User(8000 + i, "UI800"+ i +"TKN");
            UserScore userScore = new UserScore(user, new Score(1500 * i));
            level.addUserScore(userScore);
        }

        LevelDAO levelDAO = mock(LevelDAO.class);
        given(levelDAO.findById(anyInt())).willReturn(level);

        LevelService levelService = new LevelServiceImpl(null, levelDAO);
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(106);

        //when
        String listAsStringFormat = levelService.transformToCSVFormat(higherScoreByLevel);

        //then
        assertThat(listAsStringFormat, equalTo(expectedString));
    }

    private String createExpectedCsvString() {
        StringBuilder builder = new StringBuilder();

        for (int i = 16; i > 1; i--) {
            builder.append(8000 + i);
            builder.append("=");
            builder.append(1500 * i);
            builder.append(",");
        }

        String expected = builder.toString();

        return expected.substring(0, expected.length() - 1);
    }
}
