package com.king.backendkingsgame.entity;

import java.util.Objects;

public class UserScore implements Comparable<UserScore> {
    private final User user;
    private final Score score;

    public UserScore(User user, Score score) {
        this.user = user;
        this.score = score;
    }

    public Score getScore() {
        return score;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserScore userScore = (UserScore) o;
        return Objects.equals(user, userScore.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, score);
    }

    @Override
    public String toString() {
        return "UserScore{" +
                "user=" + user +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(UserScore userScore) {
        return userScore.getScore().getPoints() - this.getScore().getPoints();
    }
}
