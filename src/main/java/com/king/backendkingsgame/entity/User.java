package com.king.backendkingsgame.entity;

import java.time.LocalDateTime;
import java.util.Objects;

public class User {
    private final int id;
    private String sessionKey;
    private LocalDateTime loggedAt;

    public User(int id, String sessionKey) {
        this.id = id;
        this.sessionKey = sessionKey;
        this.loggedAt = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public LocalDateTime getLoggedAt() {
        return loggedAt;
    }

    public boolean isTokenExpired() {
        return LocalDateTime.now().minusMinutes(10).isAfter(this.loggedAt);
    }

    public void setLoggedAt(LocalDateTime loggedAt) {
        this.loggedAt = loggedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", sessionKey='" + sessionKey + '\'' +
                ", loggedAt=" + loggedAt +
                '}';
    }
}
