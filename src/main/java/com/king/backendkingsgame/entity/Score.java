package com.king.backendkingsgame.entity;

import java.util.Objects;

public class Score implements Comparable<Score>{
    private final int points;

    public Score(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return points == score.points;
    }

    @Override
    public int hashCode() {
        return Objects.hash(points);
    }

    @Override
    public int compareTo(Score score) {
        return score.getPoints() - points;
    }

    @Override
    public String toString() {
        return "Score{" +
                "points=" + points +
                '}';
    }
}
