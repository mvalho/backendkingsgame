package com.king.backendkingsgame.entity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Level {
    private int id;
    private List<UserScore> userScores;

    public Level(int id) {
        this.id = id;
        this.userScores = new LinkedList<>();
    }

    public Integer getId() {
        return id;
    }

    public void addUserScore(UserScore userScore) {
        UserScore existedUserScore = this.userScores.parallelStream().filter(us -> us.equals(userScore)).findFirst().orElse(null);

        if (existedUserScore == null) {
            this.userScores.add(userScore);
        } else if (existedUserScore.getScore().getPoints() <= userScore.getScore().getPoints()) {
            this.userScores.remove(existedUserScore);
            this.userScores.add(userScore);
        }
    }

    public List<UserScore> getUserScores() {
        Collections.sort(userScores);
        return userScores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Level level = (Level) o;
        return id == level.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", userScores=" + userScores +
                '}';
    }
}
