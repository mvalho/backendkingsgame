package com.king.backendkingsgame.handler;

public enum HandlerType {
    LOGIN, ADD_SCORE, HIGHER_SCORES, NOTHING
}
