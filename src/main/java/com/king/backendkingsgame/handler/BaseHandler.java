package com.king.backendkingsgame.handler;

import com.sun.net.httpserver.HttpExchange;

public interface BaseHandler {
    void handlerRequest(HttpExchange httpExchange);
}
