package com.king.backendkingsgame.handler.impl;

import com.king.backendkingsgame.dao.impl.UserDAOImpl;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.handler.HandlerUtil;
import com.king.backendkingsgame.handler.LoginHandler;
import com.king.backendkingsgame.service.LoginService;
import com.king.backendkingsgame.service.impl.LoginServiceImpl;
import com.sun.net.httpserver.HttpExchange;

import java.util.logging.Logger;

public class LoginHandlerImpl implements LoginHandler {
    private final static Logger LOGGER = Logger.getLogger(LoginHandler.class.getName());
    private LoginService loginService;

    public LoginHandlerImpl() {
        this.loginService = new LoginServiceImpl(UserDAOImpl.getInstance());
    }

    @Override
    public void handlerRequest(HttpExchange httpExchange) {
        if (HandlerUtil.isNotExpectedMethodType(httpExchange, "GET")) return;
        LOGGER.info("Handling Login");

        int userId = getUserId(httpExchange);
        User login = loginService.login(userId);
        String response = login.getSessionKey();

        HandlerUtil.produceResponse(httpExchange, response);
    }

    private int getUserId(HttpExchange httpExchange) {
        LOGGER.info("Extracting userId to be logged in");
        return Integer.parseInt(httpExchange.getRequestURI().toString().split("/")[1]);
    }
}
