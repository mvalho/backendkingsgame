package com.king.backendkingsgame.handler.impl;

import com.king.backendkingsgame.dao.impl.LevelDAOImpl;
import com.king.backendkingsgame.dao.impl.UserDAOImpl;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.handler.AddScoreHandler;
import com.king.backendkingsgame.handler.HandlerUtil;
import com.king.backendkingsgame.service.LevelService;
import com.king.backendkingsgame.service.LoginService;
import com.king.backendkingsgame.service.impl.LevelServiceImpl;
import com.king.backendkingsgame.service.impl.LoginServiceImpl;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class AddScoreHandlerImpl implements AddScoreHandler {
    private final static Logger LOGGER = Logger.getLogger(AddScoreHandler.class.getName());
    private LevelService levelService;
    private LoginService loginService;

    public AddScoreHandlerImpl() {
        this.levelService = new LevelServiceImpl(UserDAOImpl.getInstance(), LevelDAOImpl.getInstance());
        this.loginService = new LoginServiceImpl(UserDAOImpl.getInstance());
    }

    @Override
    public void handlerRequest(HttpExchange httpExchange) {
        if (HandlerUtil.isNotExpectedMethodType(httpExchange, "POST")) return;
        LOGGER.info("Handling Add Scores");

        String sessionKey = getSessionKey(httpExchange);
        if (!levelService.isSessionKeyInvalid(sessionKey)) {
            int levelId = HandlerUtil.getLevelId(httpExchange);
            User user = loginService.getUserBySessionKey(sessionKey);
            int score = getScorePoints(httpExchange);

            LOGGER.info("SessionKey valid for user" + user.getId());

            if (score >= 0) {
                levelService.addLevelUserScore(levelId, user, score);
            }
        }

        HandlerUtil.produceResponse(httpExchange);
    }

    private int getScorePoints(HttpExchange httpExchange) {
        LOGGER.info("Extracting Score Points");
        InputStreamReader reader;
        try {
            reader = new InputStreamReader(httpExchange.getRequestBody(), StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(reader);
            return Integer.parseInt(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private String getSessionKey(HttpExchange httpExchange) {
        LOGGER.info("Extracting sessionKey");
        String query = httpExchange.getRequestURI().getQuery();
        if (!query.isEmpty() && query.contains("=")) {
            String[] querySplit = query.split("=");
            return querySplit.length == 2 ? querySplit[1] : null;
        }

        return null;
    }
}
