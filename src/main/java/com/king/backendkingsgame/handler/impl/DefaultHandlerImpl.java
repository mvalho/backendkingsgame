package com.king.backendkingsgame.handler.impl;

import com.king.backendkingsgame.handler.DefaultHandler;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;

public class DefaultHandlerImpl implements DefaultHandler {
    @Override
    public void handlerRequest(HttpExchange httpExchange) {
        try {
            httpExchange.sendResponseHeaders(404, 0);
            httpExchange.getResponseBody().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
