package com.king.backendkingsgame.handler.impl;

import com.king.backendkingsgame.dao.impl.LevelDAOImpl;
import com.king.backendkingsgame.dao.impl.UserDAOImpl;
import com.king.backendkingsgame.entity.UserScore;
import com.king.backendkingsgame.handler.HandlerUtil;
import com.king.backendkingsgame.handler.HigherScoresHandler;
import com.king.backendkingsgame.service.LevelService;
import com.king.backendkingsgame.service.impl.LevelServiceImpl;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.util.List;
import java.util.logging.Logger;

public class HigherScoresHandlerImpl implements HigherScoresHandler {
    private static final Logger LOGGER = Logger.getLogger(HigherScoresHandler.class.getName());
    private LevelService levelService;

    public HigherScoresHandlerImpl() {
        this.levelService = new LevelServiceImpl(UserDAOImpl.getInstance(), LevelDAOImpl.getInstance());
    }

    @Override
    public void handlerRequest(HttpExchange httpExchange) {
        if (HandlerUtil.isNotExpectedMethodType(httpExchange, "GET")) return;
        LOGGER.info("Handling Higher Scores");

        int levelId = HandlerUtil.getLevelId(httpExchange);
        List<UserScore> higherScoreByLevel = levelService.getHigherScoreByLevel(levelId);
        String csvFormat = "";

        if (!higherScoreByLevel.isEmpty()) {
            csvFormat = levelService.transformToCSVFormat(higherScoreByLevel);
        }

        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Content-Type", "text/csv");

        HandlerUtil.produceResponse(httpExchange, csvFormat);
    }
}
