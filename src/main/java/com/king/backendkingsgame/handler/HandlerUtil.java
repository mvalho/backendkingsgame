package com.king.backendkingsgame.handler;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

public class HandlerUtil {
    private final static Logger LOGGER = Logger.getLogger(HandlerUtil.class.getName());

    private HandlerUtil() {
    }

    public static boolean isNotExpectedMethodType(HttpExchange httpExchange, String methodType) {
        if (!httpExchange.getRequestMethod().equals(methodType)) {
            LOGGER.warning("The Method " + methodType + " is not allowed for this request.");
            try {
                httpExchange.sendResponseHeaders(405, 0);
                httpExchange.getResponseBody().close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }
        return false;
    }

    public static int getLevelId(HttpExchange httpExchange) {
        LOGGER.info("Extracting level id");
        return Integer.parseInt(httpExchange.getRequestURI().toString().split("/")[1]);
    }

    public static void produceResponse(HttpExchange httpExchange, String response) {
        LOGGER.info("Sending response for request");
        try {
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void produceResponse(HttpExchange httpExchange) {
        try {
            httpExchange.sendResponseHeaders(200, 0);
            httpExchange.getResponseBody().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}