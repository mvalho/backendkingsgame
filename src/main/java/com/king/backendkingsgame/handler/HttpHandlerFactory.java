package com.king.backendkingsgame.handler;

import com.king.backendkingsgame.handler.impl.AddScoreHandlerImpl;
import com.king.backendkingsgame.handler.impl.DefaultHandlerImpl;
import com.king.backendkingsgame.handler.impl.HigherScoresHandlerImpl;
import com.king.backendkingsgame.handler.impl.LoginHandlerImpl;
import com.sun.net.httpserver.HttpExchange;

import java.util.logging.Logger;

public class HttpHandlerFactory {
    private final static Logger LOGGER = Logger.getLogger(HttpHandlerFactory.class.getName());

    public static void getHandler(HttpExchange httpExchange) {
        HandlerType handlerType = getHandlerType(httpExchange);

        LOGGER.info("Requesting handler for " + handlerType);
        switch (handlerType) {
            case LOGIN:
                LoginHandler loginHandler = new LoginHandlerImpl();
                loginHandler.handlerRequest(httpExchange);
                break;
            case ADD_SCORE:
                AddScoreHandler addScoreHandler = new AddScoreHandlerImpl();
                addScoreHandler.handlerRequest(httpExchange);
                break;
            case HIGHER_SCORES:
                HigherScoresHandler higherScoresHandler = new HigherScoresHandlerImpl();
                higherScoresHandler.handlerRequest(httpExchange);
                break;
            default:
                DefaultHandler defaultHandler = new DefaultHandlerImpl();
                defaultHandler.handlerRequest(httpExchange);
                break;
        }
    }

    private static HandlerType getHandlerType(HttpExchange httpExchange) {
        String requestURI = httpExchange.getRequestURI().toString();
        if (requestURI.matches("/[0-9]+/login")) {
            return HandlerType.LOGIN;
        } else if (requestURI.matches("/[0-9]+/score\\?sessionkey=[A-Z]{10}")) {
            return HandlerType.ADD_SCORE;
        } else if (requestURI.matches("/[0-9]+/highscorelist")) {
            return HandlerType.HIGHER_SCORES;
        }
        return HandlerType.NOTHING;
    }
}