package com.king.backendkingsgame.dao;

import com.king.backendkingsgame.entity.User;

public interface UserDAO {
    User save(User user);

    User findByID(int id);

    int size();

    User findBySessionKey(String sessionKey);
}
