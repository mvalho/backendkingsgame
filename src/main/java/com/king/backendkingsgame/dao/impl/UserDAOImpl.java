package com.king.backendkingsgame.dao.impl;

import com.king.backendkingsgame.dao.UserDAO;
import com.king.backendkingsgame.entity.User;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class UserDAOImpl implements UserDAO {
    private final static Logger LOGGER = Logger.getLogger(UserDAO.class.getName());
    private ConcurrentHashMap<Integer, User> userQueue;

    private UserDAOImpl() {
        userQueue = new ConcurrentHashMap<>();
    }

    @Override
    public User save(User user) {
        LOGGER.info("User " + user.getId() + " saved!");
        return userQueue.put(user.getId(), user);
    }

    @Override
    public User findByID(int id) {
        return userQueue.get(id);
    }

    @Override
    public int size() {
        return userQueue.size();
    }

    @Override
    public User findBySessionKey(final String sessionKey) {
        return this.userQueue.searchValues(1, user -> user.getSessionKey().equals(sessionKey) ? user : null);
    }

    public static UserDAO getInstance() {
        return LazyHolder.INITIALIZE;
    }

    private static class LazyHolder {
        final static UserDAO INITIALIZE = new UserDAOImpl();
    }
}
