package com.king.backendkingsgame.dao.impl;

import com.king.backendkingsgame.dao.LevelDAO;
import com.king.backendkingsgame.entity.Level;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class LevelDAOImpl implements LevelDAO {
    private final static Logger LOGGER = Logger.getLogger(LevelDAO.class.getName());
    ConcurrentHashMap<Integer, Level> levelQueue;
    private LevelDAOImpl() {
        this.levelQueue = new ConcurrentHashMap<>();
    }

    @Override
    public Level save(Level level) {
        this.levelQueue.put(level.getId(), level);
        LOGGER.info("SAVED!");
        return level;
    }

    @Override
    public Level findById(Integer id) {
        return this.levelQueue.get(id);
    }

    private static class LazyHolder {
        final static LevelDAO INITIALIZE = new LevelDAOImpl();
    }

    public static LevelDAO getInstance() {
        return LazyHolder.INITIALIZE;
    }
}
