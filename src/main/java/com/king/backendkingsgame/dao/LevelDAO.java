package com.king.backendkingsgame.dao;

import com.king.backendkingsgame.entity.Level;

public interface LevelDAO {
    Level save(Level level);

    Level findById(Integer id);
}
