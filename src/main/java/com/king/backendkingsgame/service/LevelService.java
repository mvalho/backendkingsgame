package com.king.backendkingsgame.service;

import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.entity.UserScore;

import java.util.List;

public interface LevelService {
    boolean isSessionKeyInvalid(String sessionKey);

    void addLevelUserScore(int levelId, User user, int score);

    List<UserScore> getHigherScoreByLevel(int levelId);

    String transformToCSVFormat(List<UserScore> higherScoreByLevel);
}
