package com.king.backendkingsgame.service.impl;

import com.king.backendkingsgame.dao.LevelDAO;
import com.king.backendkingsgame.dao.UserDAO;
import com.king.backendkingsgame.entity.Level;
import com.king.backendkingsgame.entity.Score;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.entity.UserScore;
import com.king.backendkingsgame.service.LevelService;

import java.util.List;
import java.util.logging.Logger;

public class LevelServiceImpl implements LevelService {
    private final static Logger LOGGER = Logger.getLogger(LevelService.class.getName());
    private LevelDAO levelDAO;
    private UserDAO userDAO;

    public LevelServiceImpl(UserDAO userDAO, LevelDAO levelDAO) {
        this.userDAO = userDAO;
        this.levelDAO = levelDAO;
    }

    @Override
    public boolean isSessionKeyInvalid(String sessionKey) {
        User user = this.userDAO.findBySessionKey(sessionKey);
        return user == null || user.isTokenExpired();
    }

    @Override
    public void addLevelUserScore(int levelId, User user, int scorePoints) {
        LOGGER.info("Adding User Score to the level: " + levelId);

        Level level = this.levelDAO.findById(levelId);

        if (level == null) {

            LOGGER.info("New level... saving...");
            level = this.levelDAO.save(new Level(levelId));
        }

        Score score = new Score(scorePoints);
        UserScore userScore = new UserScore(user, score);
        level.addUserScore(userScore);

        LOGGER.info("Added score to the level");

        this.levelDAO.save(level);
    }

    @Override
    public List<UserScore> getHigherScoreByLevel(int levelId) {
        LOGGER.info("Retrieving Higher Scores for level " + levelId);
        Level level = this.levelDAO.findById(levelId);

        return level.getUserScores().size() > 15 ? level.getUserScores().subList(0, 15) : level.getUserScores();
    }

    @Override
    public String transformToCSVFormat(List<UserScore> higherScoreByLevel) {
        LOGGER.info("Transforming List of Higher Scores into CSV format");
        StringBuffer buffer = new StringBuffer();

        higherScoreByLevel.forEach(userScore -> {
            buffer.append(userScore.getUser().getId());
            buffer.append("=");
            buffer.append(userScore.getScore().getPoints());
            buffer.append(",");
        });

        String csv = buffer.toString();

        return csv.substring(0, csv.length() - 1);
    }
}
