package com.king.backendkingsgame.service.impl;

import com.king.backendkingsgame.dao.UserDAO;
import com.king.backendkingsgame.entity.User;
import com.king.backendkingsgame.service.LoginService;

import java.util.Random;
import java.util.logging.Logger;

public class LoginServiceImpl implements LoginService {
    private final static Logger LOGGER = Logger.getLogger(LoginService.class.getName());
    private UserDAO userDAO;

    public LoginServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User login(int userId) {
        LOGGER.info("Logging in with user id: " + userId);
        String sessionKey = generateSessionKey();
        User newUser = new User(userId, sessionKey);

        if(this.userDAO != null) {
            LOGGER.info("New user... Saving...");
            this.userDAO.save(newUser);
        }

        return newUser;
    }

    @Override
    public User getUserBySessionKey(String sessionKey) {
        return this.userDAO.findBySessionKey(sessionKey);
    }

    private String generateSessionKey() {
        LOGGER.info("Generating sessionKey...");
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);

        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }

        LOGGER.info("DONE!");
        return buffer.toString().toUpperCase();
    }
}
