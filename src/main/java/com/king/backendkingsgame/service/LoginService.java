package com.king.backendkingsgame.service;

import com.king.backendkingsgame.entity.User;

public interface LoginService {
    User login(int userId);

    User getUserBySessionKey(String sessionKey);
}
