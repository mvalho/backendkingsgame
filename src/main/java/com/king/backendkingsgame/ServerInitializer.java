package com.king.backendkingsgame;

import com.king.backendkingsgame.handler.HttpHandlerFactory;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class ServerInitializer {
    private final static Logger LOGGER = Logger.getLogger(ServerInitializer.class.getName());

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8081), 0);
        server.createContext("/", HttpHandlerFactory::getHandler);
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();

        LOGGER.info("==== Started server at port http://localhost:8081 ====");
    }
}
